﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    // member publics
    public float turnSpeed = 20f;
    public TextMeshProUGUI countText;
    public int count = 0;
    public bool invulnerable;
    public int invulnerableCharges = 3;
    public Slider slider;

    // member privates
    Vector3 movement;
    Animator animator;
    Quaternion rotation = Quaternion.identity;
    Rigidbody rigidbody;
    AudioSource audioSource;
    float invulnerableCooldown;
    int sliderCount;

    // Start is called before the first frame update
    void Start()
    {
        //getting reference to the Animator component
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        sliderCount = invulnerableCharges;
        SetSlider();
    }

    void FixedUpdate()
    {
        // getting values of input from the horizontal and vertical axes
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // Giving movement a vector
        movement.Set(horizontal, 0f, vertical);
        movement.Normalize();

        // detecting if player is using the horizontal or vertical axes
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        //testing if there is input from either axis
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        //sets the isWalking Animation condition to whatever is walking is
        animator.SetBool("IsWalking", isWalking);

        //detects if audio playing and then plays while player is walking and stops audio otherwise
        if(isWalking)
        {
            if(!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
        else
        {
            audioSource.Stop();
        }
  
        // calculates the vector for turning from the current forward position to movement
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, movement, turnSpeed * Time.deltaTime, 0f);

        // stores the rotation of the previous vector
        rotation = Quaternion.LookRotation(desiredForward);
    }
    private void OnAnimatorMove()
    {
        //moves the rigid bodys position by the delta movement
        rigidbody.MovePosition(rigidbody.position + movement * animator.deltaPosition.magnitude);

        //moves the rigid bodys rotation by the current rotation
        rigidbody.MoveRotation(rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        //detects if the trigger is a pickup item
        if (other.gameObject.CompareTag("Pickup"))
        {
            //deactivates the item
            other.gameObject.SetActive(false);
            //plays particles
            //increments counter
            count++;
            //applies counter to UI
            SetCountText();
        }
    }

    void SetCountText()
    {
        //sets countText's text to the count variable
        countText.text = "Keys Collected: " + count.ToString();
    }

    private void Update()
    {
        //While invulnerable...
        if(invulnerable)
        {
            //...decrements cooldown timer
            invulnerableCooldown -= Time.deltaTime;

            //when the cooldown expires...
            if(invulnerableCooldown <= 0f)
            {
                //...Turn off invulnerability
                invulnerable = false;
            }
        }
        //when space is pressed and cooldown is up, makes player invulnerable
        if(Input.GetKeyDown(KeyCode.Space) && invulnerableCooldown <= 0f && invulnerableCharges > 0)
        {
            invulnerable = true;
            invulnerableCooldown = 5f;
            //loses one charge
            invulnerableCharges--;
            sliderCount--;
            SetSlider();
        }
    }

    void SetSlider()
    {
        //sets the slider value on call
        slider.value = sliderCount;
    }
}
