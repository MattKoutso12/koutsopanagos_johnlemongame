﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    //public members
    public Transform waypoint;


    private void OnTriggerEnter(Collider other)
    {
        //detects if the collider is the player and the cooldown is off, then teleports them to linked teleporter
        if(other.gameObject.CompareTag("Player"))
        {
            Vector3 destination = new Vector3(waypoint.position.x, waypoint.position.y, waypoint.position.z);
            other.gameObject.transform.position = destination;
        }
    }
}
