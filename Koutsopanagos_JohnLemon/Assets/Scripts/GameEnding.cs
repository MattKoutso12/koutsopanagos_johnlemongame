﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{ 
    //Public members
    public float fadeDuration = 1f;
    public PlayerMovement player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public float displayImageDuration = 1f;
    public AudioSource exitAudio;
    public AudioSource caughtAudio;

    //private members
    bool isPlayerAtExit;
    float timer;
    bool isPlayerCaught;
    bool hasAudioPlayed;

    private void OnTriggerEnter(Collider other)
    {
        //sets isPlayerAtExit to true if player enters exit collider
        if(other.gameObject == player.gameObject && player.count >= 5)
        {
            isPlayerAtExit = true;
        }
    }

    public void CaughtPlayer()
    {
        isPlayerCaught = true;
    }

    private void Update()
    {
        //detects if player is at exit, then ends level
        //also detects if the player got caught, then allows level to be restarted
        //Each playing their respective music
        if(isPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if(isPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        //plays audio if audio hasnt played yet
        if(!hasAudioPlayed)
        {
            audioSource.Play();
            hasAudioPlayed = true;
        }
        //adds deltaTime to timer
        timer += Time.deltaTime;
        //fades game screen out according to timer
        imageCanvasGroup.alpha = timer / fadeDuration;

        //after the fade has completed, the game will end
        if(timer > fadeDuration + displayImageDuration)
        {
            //allows restart based on parameter
            if(doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
