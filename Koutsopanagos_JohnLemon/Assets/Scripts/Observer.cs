﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    //public members
    public Transform player;
    public GameEnding gameEnding;

    //private members
    bool isPlayerInRange;

    private void OnTriggerEnter(Collider other)
    {
        //detects if player has entered observers line of sight
        if(other.transform == player)
        {
            isPlayerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //detects if player has exited observers line of sight 
        if(other.transform == player)
        {
            isPlayerInRange = false;
        }
    }

    private void Update()
    {
        if(isPlayerInRange)
        {
            //creates a vector that is locating the players center of mass
            Vector3 direction = player.position - transform.position + Vector3.up;

            //creates a new ray that uses the previously made vector and the observer as endpoints
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            //detects if the previously made ray is touching an object
            if (Physics.Raycast(ray, out raycastHit))
            {
                //detects if that object is the player
                if(raycastHit.collider.transform == player)
                {
                    //collects reference to the playermovement script attached to john
                    PlayerMovement movement = player.GetComponent<PlayerMovement>();

                    //if player is not is not invulnerable
                    if (!movement.invulnerable)
                    {
                        gameEnding.CaughtPlayer();
                    }
                }
            }
        }
    }
}
